package com.pandz.springauthtoken.repository;

import com.pandz.springauthtoken.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
  @Query(value = "select * from users where username = ?1", nativeQuery = true)
  User findByUsername(String username);

  @Query(value = "select count(*) from users where username = ?1", nativeQuery = true)
  Long countByUsername(String username);

  @Query(value = "select count(*) from users where email = ?1", nativeQuery = true)
  Long countByEmail(String email);
}
