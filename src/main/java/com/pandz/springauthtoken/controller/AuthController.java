package com.pandz.springauthtoken.controller;

import com.pandz.springauthtoken.config.JwtUtil;
import com.pandz.springauthtoken.model.ERole;
import com.pandz.springauthtoken.model.Role;
import com.pandz.springauthtoken.model.User;
import com.pandz.springauthtoken.payload.request.LoginReq;
import com.pandz.springauthtoken.payload.request.RegisterReq;
import com.pandz.springauthtoken.payload.response.JwtRes;
import com.pandz.springauthtoken.payload.response.MessageRes;
import com.pandz.springauthtoken.repository.RoleRepository;
import com.pandz.springauthtoken.repository.UserRepository;
import com.pandz.springauthtoken.service.UserDetailImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/auth")
@CrossOrigin(origins = "*", maxAge = 3600)
public class AuthController {
  private final static Logger log = LoggerFactory.getLogger(AuthController.class);

  @Autowired
  AuthenticationManager authenticationManager;

  @Autowired
  UserRepository userRepository;

  @Autowired
  RoleRepository roleRepository;

  @Autowired
  PasswordEncoder passwordEncoder;

  @Autowired
  JwtUtil jwtUtil;

  @PostMapping("/login")
  public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginReq loginReq) {
    log.info("### Start login");
    log.info("### Request username: " + loginReq.getUsername() + ". Password: " + passwordEncoder.encode(loginReq.getPassword()));

    Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
        loginReq.getUsername(),
        loginReq.getPassword()
    ));

    SecurityContextHolder.getContext().setAuthentication(authentication);
    String jwt = jwtUtil.generateJwtToken(authentication);

    log.info("JWT: " + jwt);

    UserDetailImpl userDetail = (UserDetailImpl) authentication.getPrincipal();
    List<String> roles = userDetail.getAuthorities().stream()
        .map(item -> item.getAuthority())
        .collect(Collectors.toList());

    return ResponseEntity.ok(new JwtRes(jwt, userDetail.getId(), userDetail.getUsername(), userDetail.getEmail(), roles));
  }

  @PostMapping("/register")
  public ResponseEntity<?> registerUser(@Valid @RequestBody RegisterReq registerReq) {
    log.info("### Start Register");
    log.info("### Request username: " + registerReq.getUsername());
    log.info("### Request email: " + registerReq.getEmail());
    log.info("### Request password: " + passwordEncoder.encode(registerReq.getPassword()).length());

    // validate username
    Long countUsername = userRepository.countByUsername(registerReq.getUsername());
    if (countUsername > 0) {
      return ResponseEntity.badRequest()
          .body(new MessageRes("Username is already exist"));
    }

    // validate email
    Long countEmail = userRepository.countByEmail(registerReq.getEmail());
    if (countEmail > 0) {
      return ResponseEntity.badRequest()
          .body(new MessageRes("Email has already in use"));
    }

    User user = new User(registerReq.getUsername(), registerReq.getEmail(), passwordEncoder.encode(registerReq.getPassword()), new Date());

    Set<String> setRoles = registerReq.getRoles();
    Set<Role> roles = new HashSet<>();

    if (setRoles == null) {
      Role userRole = roleRepository.findByName(ERole.ROLE_USER)
          .orElseThrow(() -> new RuntimeException("Role is not found"));
      roles.add(userRole);
    } else {
      setRoles.forEach(role -> {
        switch (role) {
          case "admin":
            Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
                .orElseThrow(() -> new RuntimeException("Role is not found"));
            roles.add(adminRole);
            break;
          case "mod":
            Role modRole = roleRepository.findByName(ERole.ROLE_MODERATOR)
                .orElseThrow(() -> new RuntimeException("Role is not found"));
            roles.add(modRole);
            break;
          default:
            Role userRole = roleRepository.findByName(ERole.ROLE_USER)
                .orElseThrow(() -> new RuntimeException("Role is not found"));
            roles.add(userRole);
        }
      });
    }
    user.setRoles(roles);
    userRepository.save(user);

    return ResponseEntity.ok(new MessageRes("User registered successfully"));
  }
}
