package com.pandz.springauthtoken.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/test")
@CrossOrigin(origins = "*", maxAge = 3600)
public class TestController {
  @GetMapping("/all")
  public String getAllAccess() {
    return "Public content example";
  }

  @GetMapping("/user")
  @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')") // need token to validate user role
  public String getUserAccess() {
    return "User content example";
  }

  @GetMapping("/moderator")
  @PreAuthorize("hasRole('MODERATOR')")
  public String getModAccess() {
    return "Moderator content example";
  }

  @GetMapping("/admin")
  @PreAuthorize("hasRole('ADMIN')")
  public String getAdminAccess() {
    return "Admin content example";
  }
}
