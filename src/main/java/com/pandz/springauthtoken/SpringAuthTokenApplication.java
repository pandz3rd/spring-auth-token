package com.pandz.springauthtoken;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringAuthTokenApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringAuthTokenApplication.class, args);
	}

}
